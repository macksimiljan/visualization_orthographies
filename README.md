Visualization of lemmas and orthographies in a tree-based manner with [d3](https://d3js.org/).

Seminar project for "Visuelle Textanalyse" (2017) in cooperation with the [DDGLC project](http://www.geschkult.fu-berlin.de/e/aegyptologie/forschung/Drittmittelprojekte/ddglc/index.html).

The presentation of the preliminary result is [here](https://gitlab.com/macksimiljan/visualization_orthographies/blob/master/visorth_pres.pdf). The final project desription is a page at the desciption path inside the app.
